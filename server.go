package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"time"
)

func main() {

	mesaj("Pornim serverul...")
	ln, err := net.Listen("tcp", ":4956")
	exitOnError(err)
	for {
		mesaj("Asteptam o conexiune...")
		conn, err := ln.Accept()
		exitOnError(err)
		go mentineConexiunea(conn)
	}

}

func mentineConexiunea(conn net.Conn) {

	mesaj("")
	mesaj("Mentinem conexiunea...")
	str, err := bufio.NewReader(conn).ReadString('\n')
	exitOnError(err)
	if len(str) > 0 {
		timp := time.Now()

		conn.Write([]byte(timp.Format(time.RFC3339Nano) + "\n"))

		primit, err := time.Parse(time.RFC3339Nano, str[:len(str)-1])
		exitOnError(err)

		mesaj("Timp primit:", primit)
		mesaj("Timp Trimis:", timp)
	}

	conn.Close()
}

func mesaj(a ...interface{}) (n int, err error) {
	return fmt.Print("[S] ", fmt.Sprintln(a...))
}

func exitOnError(err error) {
	if err != nil {
		mesaj("ERR:", err)
		os.Exit(1)
	}
}
