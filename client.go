package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"time"
)

func main() {

	mesaj("Pornim Clientul")
	conn, err := net.Dial("tcp", "127.0.0.1:4956")
	exitOnError(err)

	inainte := time.Now()
	mesaj("")
	mesaj("Timpul din acest moment inaintea trimiterii mesajului catre server:  ", inainte)
	fmt.Fprintf(conn, inainte.Format(time.RFC3339Nano)+"\n")

	str, err := bufio.NewReader(conn).ReadString('\n')
	exitOnError(err)
	if len(str) < 1 {
		mesaj("ERR: Invalid string!")
		os.Exit(1)
	}

	primit, err := time.Parse(time.RFC3339Nano, str[:len(str)-1])
	exitOnError(err)
	mesaj("Timpul primit de la server:", primit)

	dupa := time.Now()
	mesaj("Timpul din acest moment dupa primirea mesajului de la server:   ", dupa)

	corectie := dupa.Sub(inainte) / 2

	mesaj("")
	mesaj("corectie: +", corectie)
	mesaj("Timpul corectat este:", primit.Add(corectie))
}

func mesaj(a ...interface{}) (n int, err error) {
	return fmt.Print("[C] ", fmt.Sprintln(a...))
}

func exitOnError(err error) {
	if err != nil {
		mesaj("ERR:", err)
		os.Exit(1)
	}
}
